<?php
/**
 * @copyright ©2018 Lu Wei
 * @author Lu Wei
 * @link http://www.luweiss.com/
 * Created by IntelliJ IDEA
 * Date Time: 2018/11/23 14:35
 */


namespace app\commands;


use yii\console\ExitCode;

class HelloController extends Controller
{
    public $message;
    public $version;

    public function options($actionID)
    {
        return ['message', 'version'];
    }

    public function optionAliases()
    {
        return [
            'm' => 'message',
            'v' => 'version',
        ];
    }

    public function actionIndex()
    {
        if ($this->version !== null) {
            $this->stdout("\nHello World v1.0.0\n");
        }
        if ($this->message !== null) {
            echo "\nYour message = $this->message\n";
        }
        return ExitCode::OK;
    }
}
