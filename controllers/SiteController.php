<?php
/**
 * @copyright ©2018 Lu Wei
 * @author Lu Wei
 * @link http://www.luweiss.com/
 * Created by IntelliJ IDEA
 * Date Time: 2018/11/23 14:20
 */


namespace app\controllers;


class SiteController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
